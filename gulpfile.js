/**
 * @file
 * Contains gulp tasks.
 */

'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');

// Compile.
function compileSass(path, stats) {
  gulp.src(path)
    .pipe(sourcemaps.init())
    .pipe(sass({
      noCache: true,
      outputStyle: "expanded",
      lineNumbers: false,
      loadPath: './assets/css/*',
      sourceMap: true
    }).on('error', sass.logError))
    .pipe(autoprefixer({
      grid: false,
      browsers: ['> 1%', 'last 2 versions']
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('assets/css'))
    .pipe(notify({
      title: "SCSS",
      message: "Compiled " + path,
      onLast: true
    }));
}

// Compile and watch.
gulp.task('watch', function () {
  gulp.watch(['assets/scss/**/*.scss'])
    .on("change", function (path, stats) {
      compileSass(path, stats);
    })
    .on("add", function (path, stats) {
      compileSass(path, stats);
    });
});

// Default task.
gulp.task('default', gulp.series('watch'));
