(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.pg_default_slideshow = {
    attach: function (context, settings) {
      $('.pt--slideshow').once('paragraph-slideshow').each(function () {

        var $paragraph = $(this);
        var $paragraph_id = $paragraph.find('.pg-slideshow').attr('id');

        $('#' + $paragraph_id + ' > .field__items').slick({
          infinite: true,
          autoplay: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          fade: false,
        });

      });
    }
  }
})(jQuery, Drupal);
